package com.darryring.libchat.call;

/**
 * Created by hljdrl on 16/4/11.
 */
public interface OkCall {

    void ok();
    void error(int what,String error,Object obj);
}
