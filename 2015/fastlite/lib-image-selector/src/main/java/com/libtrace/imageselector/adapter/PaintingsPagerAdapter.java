package com.libtrace.imageselector.adapter;

import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.view.ViewGroup;

import com.alexvasilkov.gestures.views.GestureImageView;
import com.alexvasilkov.gestures.views.utils.RecyclePagerAdapter;
import com.libtrace.imageselector.LibImagePreviewActivity;
import com.libtrace.imageselector.R;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.File;
import java.util.List;

public class PaintingsPagerAdapter extends RecyclePagerAdapter<PaintingsPagerAdapter.ViewHolder> {

    private final ViewPager mViewPager;
    private final List<String> mImageUris;
    private final GestureSettingsSetupListener mSetupListener;
    ImageLoader imageLoader;
    private final  int imageUriType;
    public PaintingsPagerAdapter(ViewPager pager, List<String> images,
                                 GestureSettingsSetupListener listener,int uriType) {
        mViewPager = pager;
        mImageUris = images;
        mSetupListener = listener;
        imageUriType = uriType;
        imageLoader = ImageLoader.getInstance();
    }

    @Override
    public int getCount() {
        return mImageUris.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup container) {
        ViewHolder holder = new ViewHolder(container);
        holder.image.getController().enableScrollInViewPager(mViewPager);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (mSetupListener != null) mSetupListener.onSetupGestureView(holder.image);
        String _uri = mImageUris.get(position);
        if(imageUriType== LibImagePreviewActivity.URI_TYPE_FILE) {
            _uri =  Uri.fromFile(new File(mImageUris.get(position))).toString();
        }else if(imageUriType==LibImagePreviewActivity.URI_TYPE_HTTP){
            //http
        }
        holder.image.setImageResource(R.drawable.default_error);
        imageLoader.displayImage(_uri,holder.image);
    }

    public static GestureImageView getImage(RecyclePagerAdapter.ViewHolder holder) {
        return ((ViewHolder) holder).image;
    }


    static class ViewHolder extends RecyclePagerAdapter.ViewHolder {
        public final GestureImageView image;

        public ViewHolder(ViewGroup container) {
            super(new GestureImageView(container.getContext()));
            image = (GestureImageView) itemView;
        }
    }

}
