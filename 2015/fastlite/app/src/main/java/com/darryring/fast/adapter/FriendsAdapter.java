package com.darryring.fast.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.darryring.fast.R;
import com.darryring.fast.util.UriForamt;
import com.darryring.libcore.util.StringUtil;
import com.darryring.libmodel.entity.FriendsEntity;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hljdrl on 16/5/27.
 */
public class FriendsAdapter extends ArrayAdapter<FriendsEntity> {


    LayoutInflater inflater;
    ImageLoader imageLoader;
    DisplayImageOptions mAvatorOptions;
    DisplayImageOptions mImageOptions;
    Resources mRes;
    List<Bitmap> mbitmaps = new ArrayList<Bitmap>();
    private String TAG="TAG";
    public FriendsAdapter(Context context, List<FriendsEntity> objects) {
        super(context, 0, objects);
        inflater = LayoutInflater.from(context);
        mRes = context.getResources();
        imageLoader = ImageLoader.getInstance();
        mAvatorOptions = new DisplayImageOptions.Builder().cacheOnDisk(true).build();
        mImageOptions = new DisplayImageOptions.Builder().cacheOnDisk(true)
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .bitmapConfig(Bitmap.Config.ARGB_4444)
                .build();
    }

    @Override
    public View getView(int position, View _view, ViewGroup parent) {
        FriendsEntity _item = getItem(position);
        ViewTag _mTag = null;
        if (_view == null) {
            _view = inflater.inflate(R.layout.item_listview_friends, null);
            _mTag = new ViewTag();
            _mTag.avator = (ImageView) _view.findViewById(R.id.imageview_avator);
            _mTag.name = (TextView) _view.findViewById(R.id.textview_name);
            _mTag.time = (TextView) _view.findViewById(R.id.textview_time);
            _mTag.body = (TextView) _view.findViewById(R.id.textview_body);
            _mTag.grid_images = new ArrayList<ImageView>();
            _mTag.grid_images.add((ImageView) _view.findViewById(R.id.grid_image_1));
            _mTag.grid_images.add((ImageView) _view.findViewById(R.id.grid_image_2));
            _mTag.grid_images.add((ImageView) _view.findViewById(R.id.grid_image_3));
            _mTag.grid_images.add((ImageView) _view.findViewById(R.id.grid_image_4));
            _mTag.grid_images.add((ImageView) _view.findViewById(R.id.grid_image_5));
            _mTag.grid_images.add((ImageView) _view.findViewById(R.id.grid_image_6));
            _mTag.grid_images.add((ImageView) _view.findViewById(R.id.grid_image_7));
            _mTag.grid_images.add((ImageView) _view.findViewById(R.id.grid_image_8));
            _mTag.grid_images.add((ImageView) _view.findViewById(R.id.grid_image_9));
            _view.setTag(_mTag);
        } else {
            _mTag = (ViewTag) _view.getTag();
        }
        //============================================================================
        _mTag.name.setText(_item.getName());
        _mTag.time.setText(_item.getTime());
        _mTag.body.setText(_item.getBody());
        //----------
        if(StringUtil.isNotNull(_item.getIcon())){
            loadUserAvator(UriForamt.formatUriHttp(_item.getIcon()),_mTag);
        }else{
            loadUserAvator(UriForamt.formatUriDrawable(R.drawable.ic_user_avator),_mTag);
        }
        //----------
        List<String> imgs = _item.getImages();
        for (int i = 0; i < 9; i++) {

            if (imgs!=null && i < imgs.size()) {
                String _uri = imgs.get(i);
                final ImageView _imageView = _mTag.grid_images.get(i);
                _imageView.setVisibility(View.VISIBLE);
                imageLoader.displayImage(UriForamt.formatUriHttp(_uri),_imageView,mImageOptions,new SimpleImageLoadingListener(){
                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        super.onLoadingComplete(imageUri, view, loadedImage);
                        mbitmaps.add(loadedImage);
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        super.onLoadingFailed(imageUri, view, failReason);
                        Log.i(TAG,"onLoadingFailed-->Image....");
                    }
                });
            }else{
                _mTag.grid_images.get(i).setVisibility(View.GONE);
            }
        }
        //============================================================================
        return _view;
    }

    private void loadUserAvator(String iconUrl,final ViewTag viewTag) {
        ImageSize _avatorSize = new ImageSize(viewTag.avator.getWidth(),viewTag.avator.getHeight());
        imageLoader.loadImage(iconUrl,_avatorSize,mAvatorOptions,new SimpleImageLoadingListener(){
            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                super.onLoadingComplete(imageUri, view, loadedImage);
                RoundedBitmapDrawable roundDrawable = RoundedBitmapDrawableFactory.create(mRes,loadedImage);
                roundDrawable.setCornerRadius(viewTag.avator.getWidth()/2);
                viewTag.avator.setImageDrawable(roundDrawable);
                mbitmaps.add(loadedImage);
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                super.onLoadingFailed(imageUri, view, failReason);
                Log.i(TAG,"onLoadingFailed-->UserAvator....");
            }
        });
    }

    public void recycledBitmaps() {
        List<Bitmap> _copy = new ArrayList<Bitmap>(mbitmaps);
        mbitmaps.clear();
        for(Bitmap _bm:_copy){
            if(_bm!=null &&! _bm.isRecycled()){
                _bm.recycle();
                _bm = null;
            }
        }
    }

    static class ViewTag {
        /**
         * 头像
         */
        public ImageView avator;
        /**
         * 名字
         */
        public TextView name;
        /**
         * 时间
         */
        public TextView time;
        /**
         * 正文
         */
        public TextView body;
        /**
         * 图片List
         */
        public List<ImageView> grid_images;

    }
}
