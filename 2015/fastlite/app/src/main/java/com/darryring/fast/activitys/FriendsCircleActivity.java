package com.darryring.fast.activitys;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;

import com.darryring.fast.R;
import com.darryring.fast.adapter.FriendsAdapter;
import com.darryring.fast.base.BaseAppCompatActivity;
import com.darryring.fast.theme.FastTheme;
import com.darryring.libcore.Fast;
import com.darryring.libmodel.entity.FriendsEntity;
import com.darryring.libmodel.entity.theme.ThemeEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * 朋友圈页面
 */
public class FriendsCircleActivity extends BaseAppCompatActivity implements View.OnClickListener{

    private String TAG="FriendsCircleActivity";

    ImageView mImageViewFriends;
    ImageView mImageViewAvator;
    ListView mListView;
    String imageBackgroundUri="http://img.zcool.cn/community/01d4d756e6c00232f875520fa3f04f.jpg@900w_1l_2o";
    Button btn_avator;
    List<FriendsEntity> datalist  = new ArrayList<FriendsEntity>();
    FriendsAdapter mAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FastTheme.fastTheme.setupTheme(this, ThemeEntity.TYPE_FRIENDS_NO_ACTION_BAR_THEME);
        super.onCreate(savedInstanceState);
        Fast.logger.i(TAG,"onCreate....");
        setContentView(R.layout.activity_friends_circle);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //
        mListView = (ListView) findViewById(R.id.listview);
        View headerView = getLayoutInflater().inflate(R.layout.header_friends_image,mListView,false);
        mListView.addHeaderView(headerView);
        mImageViewFriends = (ImageView) findViewById(R.id.imageview_friends_background);
        mImageViewAvator = (ImageView) findViewById(R.id.imageview_avator);

        btn_avator = (Button) findViewById(R.id.button_avator);
        btn_avator.setOnClickListener(this);
        //

        if(mAdapter==null){
            datalist.addAll(testdatalist());
            mAdapter = new FriendsAdapter(this,datalist);
            mListView.setAdapter(mAdapter);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home)
        {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.button_avator){

        }
    }

    List<FriendsEntity> testdatalist() {
        List<FriendsEntity> _copy = new ArrayList<FriendsEntity>();

        FriendsEntity _new1 = new FriendsEntity();
        //------------------------------------------------------------------
        _new1.setName("哈哈");
        _new1.setTime("2016-05-01 10:10:12");
        _new1.setBody("喜欢初夏透着光的树林，小草和逆光的树叶，一切都那么美好。喜欢初夏透着光的树林，小草和逆光的树叶，一切都那么美好。喜欢初夏透着光的树林，小草和逆光的树叶，一切都那么美好。喜欢初夏透着光的树林，小草和逆光的树叶，一切都那么美好。喜欢初夏透着光的树林，小草和逆光的树叶，一切都那么美好。");
        _new1.setIcon("http://img.zcool.cn/community/04cb075540b8250000017c50e88fd3.jpg@64w_64h_2o");
        List<String> _imgs = new ArrayList<String>();
        _imgs.add("http://img.zcool.cn/community/0316e025745222c6ac72525ae9158fa.jpg@250w_188h_1c_1e_2o");
        _imgs.add("http://img.zcool.cn/community/0315185574510cc000000a4290aaf7e.jpg@250w_188h_1c_1e_2o");
        _imgs.add("http://img.zcool.cn/community/03290f457450d6c6ac72525aea39eff.jpg@250w_188h_1c_1e_2o");
        _imgs.add("http://img.zcool.cn/community/0318802574517f932f875a429f06dea.jpg@250w_188h_1c_1e_2o");
        _imgs.add("http://img.zcool.cn/community/031e3705743c05b32f8759a3ef6d4a8.jpg@250w_188h_1c_1e_2o");
        _imgs.add("http://img.zcool.cn/community/031f6d15743b0dd6ac725550b61c336.jpg@250w_188h_1c_1e_2o");
        _imgs.add("http://img.zcool.cn/community/032cb2a5743a7d532f8759a3e20ad2c.jpg@250w_188h_1c_1e_2o");
        _imgs.add("http://img.zcool.cn/community/03103155742767c6ac725ac3f4da1f7.jpg@250w_188h_1c_1e_2o");
        _imgs.add("http://img.zcool.cn/community/031350857426dea6ac725ac3fb6411f.jpg@250w_188h_1c_1e_2o");
        _new1.setImages(_imgs);
        _copy.add(_new1);
        //---------------------------------------------------------------
        //------------------------------------------------------------------
        FriendsEntity _new2 = new FriendsEntity();
        _new2.setTime("2016-05-05 12:12:12");
        _new2.setName("嘻嘻");
        _new2.setBody("主形象虾小米来到大阪参加SUMMER SONIC 2015音乐节，并因此遇到了同样是音乐发烧友的——喜欢泡汤的章鱼太郎、喜欢吃寿司的鲤鱼君以及喜欢园艺的螃蟹君。在一起感受SUMMER SONIC 2015热辣音乐的同时，虾小米与他们一同体验了日本的各种风情、美食、文化，并建立深厚的友谊。");
        _new2.setIcon("http://img.zcool.cn/community/04332c5742e6fa6ac725550b6621cf.jpg@64w_64h_2o");
        List<String> _imgs2 = new ArrayList<String>();
        _imgs2.add("http://img.zcool.cn/community/0312ebb573eeabe000000ac3f8f2e04.jpg@250w_188h_1c_1e_2o");
        _imgs2.add("http://img.zcool.cn/community/031e22b573df9c132f8757cb91fd88e.jpg@250w_188h_1c_1e_2o");
        _imgs2.add("http://img.zcool.cn/community/031f1d6573e75c232f8757cb944c395.jpg@250w_188h_1c_1e_2o");
        _imgs2.add("http://img.zcool.cn/community/031c941573dbf586ac7253f9afb8e29.jpg@250w_188h_1c_1e_2o");
        _new2.setImages(_imgs2);
        _copy.add(_new2);
        //---------------------------------------------------------------
        _copy.add(_new1);
        _copy.add(_new2);
        _copy.add(_new1);
        _copy.add(_new2);
        _copy.add(_new1);
        _copy.add(_new2);
        _copy.add(_new1);
        _copy.add(_new2);
        //
        _copy.add(_new1);
        _copy.add(_new2);
        _copy.add(_new1);
        _copy.add(_new2);
        _copy.add(_new1);
        _copy.add(_new2);
        _copy.add(_new1);
        _copy.add(_new2);
        return _copy;
    }

    @Override
    protected void onPause() {
        super.onPause();
        Fast.logger.i(TAG,"onPause....");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Fast.logger.i(TAG,"onDestroy....");
    }
}
