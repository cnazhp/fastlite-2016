package com.darryring.fast.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.darryring.fast.R;

import java.util.List;

/**
 * Created by hljdrl on 16/4/11.
 */
public class PinYinAdapter extends ArrayAdapter<String> {

    LayoutInflater inflater;
    public PinYinAdapter(Context context, List<String> objects) {
        super(context, 0, objects);
        inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View _view, ViewGroup parent) {
        ViewHolder _viewHolder = null;
        if(_view==null){
            _view = inflater.inflate(R.layout.item_pinyin,null);
            _viewHolder = new ViewHolder();
            _viewHolder.textPinYin = (TextView) _view.findViewById(R.id.text_pinyin);
            _viewHolder.name = (TextView) _view.findViewById(R.id.tv_ct_name);
            _viewHolder.sign = (TextView) _view.findViewById(R.id.tv_ct_sign);
            _view.setTag(_viewHolder);
        }else{
            _viewHolder = (ViewHolder) _view.getTag();
        }
        String _ct = getItem(position);
        _viewHolder.name.setText(_ct);
        return _view;
    }
    private static class ViewHolder{
        public TextView textPinYin;
        public TextView  name;
        public TextView  sign;
    }
}
