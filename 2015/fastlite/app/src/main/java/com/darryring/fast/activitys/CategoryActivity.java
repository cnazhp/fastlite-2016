package com.darryring.fast.activitys;

import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.darryring.fast.R;
import com.darryring.fast.adapter.CategoryAdapter;
import com.darryring.fast.base.BaseAppCompatActivity;
import com.darryring.fast.fragment.BaseFragment;
import com.darryring.fast.fragment.ContactFragment;
import com.darryring.fast.fragment.SessionFragment;
import com.darryring.fast.theme.FastTheme;
import com.darryring.fast.util.DrawableUtil;
import com.darryring.libmodel.entity.theme.ThemeEntity;
import com.darryring.libview.PagerSlidingTabStrip;

import java.util.ArrayList;
import java.util.List;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class CategoryActivity extends BaseAppCompatActivity {

    private String TAG="CategoryActivity";
    /**
     *
     */
    ViewPager mViewPager;
    /**
     *
     */
    PagerSlidingTabStrip mPagerSlidingTabStrip;
    /**
     *
     */
    CategoryAdapter mCategoryAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FastTheme.fastTheme.setupTheme(this, ThemeEntity.TYPE_NO_ACTION_BAR);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //===============================================================================
        mViewPager = (ViewPager) findViewById(R.id.viewpager);
        mPagerSlidingTabStrip = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        //===============================================================================
        if(FastTheme.fastTheme.theme()!=null){
            TypedArray ta = getTheme().obtainStyledAttributes(R.styleable.FastColorTheme);
            int _defaultColor =getResources().getColor(R.color.libres_menu_text_normal);
            int _selectColor = ta.getColor(R.styleable.FastColorTheme_colorFastNomal,0);

            ColorStateList colorStateList = DrawableUtil.createTabBarColorStateList(_defaultColor,_selectColor);
            mPagerSlidingTabStrip.setTabTextColorStateList(colorStateList);
            mPagerSlidingTabStrip.setIndicatorColor(_selectColor);
            ta.recycle();
        }
        //==============================================================================
        List<BaseFragment> fragments = new ArrayList<>();
        //
        fragments.add(new SessionFragment("精选"));
        fragments.add(new ContactFragment("体育"));
        fragments.add(new SessionFragment("电影"));
        fragments.add(new ContactFragment("视频"));
        fragments.add(new SessionFragment("哈哈"));
        fragments.add(new ContactFragment("滴滴"));
        //
        mCategoryAdapter = new CategoryAdapter(getSupportFragmentManager(),fragments);
        mViewPager.setAdapter(mCategoryAdapter);
        mPagerSlidingTabStrip.setViewPager(mViewPager);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home)
        {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
