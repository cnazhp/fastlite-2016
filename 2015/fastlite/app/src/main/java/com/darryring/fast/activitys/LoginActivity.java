package com.darryring.fast.activitys;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.darryring.fast.R;
import com.darryring.fast.base.BaseAppCompatActivity;
import com.darryring.fast.theme.FastTheme;
import com.darryring.fast.util.DrawableUtil;
import com.darryring.fast.util.InputUtil;
import com.darryring.libcore.Fast;
import com.darryring.libmodel.entity.theme.ThemeEntity;

import net.steamcrafted.loadtoast.LoadToast;


/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends BaseAppCompatActivity implements OnClickListener {

    public static String SAVE_LOGIN_STATE = "com.darryring.fast.activitys.LoginActivity.KEY_LOGIN";

    // UI references.
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;

    //
    TextView tvYouPhoto;
    //
    TextView tvAccountWeiXin;
    TextView tvAccountWeiBo;
    TextView tvAccountQQ;
    int themeColor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FastTheme.fastTheme.setupTheme(this, ThemeEntity.TYPE_THEME);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        findViewById(R.id.input_click_layout).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                InputUtil.hideInput(LoginActivity.this);
            }
        });
        tvYouPhoto = (TextView) findViewById(R.id.tv_your_photo);
        tvAccountWeiXin = (TextView) findViewById(R.id.tv_account_weixin);
        tvAccountWeiBo = (TextView) findViewById(R.id.tv_account_weibo);
        tvAccountQQ = (TextView) findViewById(R.id.tv_account_qq);
        //-------------------------
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);

        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    return true;
                }
                return false;
            }
        });
        tvYouPhoto.setOnClickListener(this);
        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        //
        final Drawable _youPhotoDrawable = tvYouPhoto.getBackground();

        //
        final Drawable originalDrawable = mEmailSignInButton.getBackground();

        TypedArray ta = getTheme().obtainStyledAttributes(R.styleable.FastColorTheme);
        int _pressedColor = ta.getColor(R.styleable.FastColorTheme_colorFastPressed,0);
        int _nomailColor = ta.getColor(R.styleable.FastColorTheme_colorFastNomal,0);
        themeColor = _nomailColor;
        //
        ColorStateList colorStateList = DrawableUtil.createButtonColorStateList(_nomailColor,_pressedColor);

        final Drawable wrappedDrawable = DrawableUtil.tintDrawable(originalDrawable,colorStateList);
        mEmailSignInButton.setBackgroundDrawable(wrappedDrawable);
        //
        mEmailSignInButton.setOnClickListener(this);
        //
        final Drawable  _tintYouPhtoDrawable = DrawableUtil.tintDrawable(_youPhotoDrawable,colorStateList);
        tvYouPhoto.setBackgroundDrawable(_tintYouPhtoDrawable);
        //
        final Drawable  _tintWeiXinDrawable = DrawableUtil.tintDrawable(tvAccountWeiXin.getBackground(),colorStateList);
        final Drawable  _tintWeiBoDrawable = DrawableUtil.tintDrawable(tvAccountWeiBo.getBackground(),colorStateList);
        final Drawable  _tintQQDrawable = DrawableUtil.tintDrawable(tvAccountQQ.getBackground(),colorStateList);
        tvAccountWeiXin.setBackgroundDrawable(_tintWeiXinDrawable);
        tvAccountWeiBo.setBackgroundDrawable(_tintWeiBoDrawable);
        tvAccountQQ.setBackgroundDrawable(_tintQQDrawable);

        //
        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
        ta.recycle();
    }



    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public void onClick(final View view) {
        if(view.getId()==R.id.email_sign_in_button){
            final LoadToast lt = new LoadToast(this).setProgressColor(themeColor).setText("login....").setTranslationY(100).show();
            lt.show();
            view.postDelayed(new Runnable() {
                @Override
                public void run() {
                    lt.success();
                     view.postDelayed(new Runnable() {
                         @Override
                         public void run() {
                             loginOk();
                         }
                     },1000);
                }
            },2000);

//            String _user = mEmailView.getText().toString();
//            String _pw = mPasswordView.getText().toString();
//            if(!StringUtil.isNull(_user)&&!StringUtil.isNull(_pw)){
//                showProgress(true);
//                if(LibChat.chatClient!=null){
//                    LibChat.chatClient.loginAsyn(_user, StringUtil.Md5(_pw), new OkCall() {
//                        @Override
//                        public void ok() {
//                            loginOk();
//                        }
//                        @Override
//                        public void error(int what, String error, Object obj) {
//                            loginError();
//                        }
//                    });
//                }
//
//            }

        }else if(view.getId()==R.id.tv_your_photo){

        }
    }

    private void loginOk() {
        if(!isFinishing()){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Fast.kvCache.saveToFileCache(SAVE_LOGIN_STATE,"true");
                    Intent mIm = new Intent(LoginActivity.this,HomeActivity.class);
                    startActivity(mIm);
                    finish();
                }
            });
        }
    }

    private void loginError() {
        if(!isFinishing()){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showProgress(false);
                }
            });
        }
    }
}

