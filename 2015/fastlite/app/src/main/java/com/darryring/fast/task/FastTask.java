package com.darryring.fast.task;

import android.util.Log;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by hljdrl on 16/7/9.
 */
public class FastTask {
    /**
     * FAST主线程,负责数据CRUD,单线程队列执行,没有新的线程进来则会等待
     */
    public static final String TAG_FAST_TASK = "TAG_MAIN_THREAD_FAST";
    /**
     * Dao主线程,负责数据CRUD,单线程队列执行,没有新的线程进来则会等待
     */
    public static final String TAG_DAO_TASK = "TAG_MAIN_THREAD_DAO";
    /**
     * Http主线程,负责网络请求,单线程队列执行,没有新的线程进来则会等待
     */
    public static final String TAG_DAO_HTTP = "TAG_MAIN_THREAD_HTTP";

    private static FastTask mFastTask = null;

    public static FastTask getInstance(){
        if(mFastTask==null){
            mFastTask = new FastTask();
        }
        return mFastTask;
    }
    Map<String,FastQueueThread> mCAHCE_MAP = new HashMap<String,FastQueueThread>();
    String TAG="FastTask";
    private FastTask(){

    }
    public void shutdown(){
        Log.i(TAG,"shutdown---->");
            if(mCAHCE_MAP.size()>0){
                Collection<FastQueueThread> _mainThreads = mCAHCE_MAP.values();
                mCAHCE_MAP.clear();
                for(FastQueueThread _main:_mainThreads){
                    _main.quit();
                }
            }
    }
    /**
     * @param tagName
     * @param _runnable
     */
    public final void postMainRunnable(String tagName,Runnable _runnable){
        Log.i(TAG,"postMainRunnable---->Name="+tagName);
            if(mCAHCE_MAP.containsKey(tagName)){
                FastQueueThread _task = mCAHCE_MAP.get(tagName);
                _task.postRunnable(_runnable);
            }else{
                FastQueueThread _newTask = new FastQueueThread();
                _newTask.setName(tagName);
                _newTask.postRunnable(_runnable);
                _newTask.start();
                mCAHCE_MAP.put(tagName,_newTask);
            }
    }
}
