package com.darryring.fast.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.darryring.fast.R;
import com.darryring.libmodel.entity.MenuEntity;

import java.util.List;

/**
 * Created by hljdrl on 16/5/21.
 */
public class MenuAdapter extends ArrayAdapter<MenuEntity> {

    LayoutInflater inflater;
    public MenuAdapter(Context context, List<MenuEntity> objects) {
        super(context, 0, objects);
        inflater = LayoutInflater.from(context);
    }


    @Override
    public View getView(int position, View _view, ViewGroup parent) {
        MenuEntity _menu = getItem(position);
        ViewTag _tag = null;
        if(_view==null){
            _view = inflater.inflate(R.layout.item_user_menu,null);
            _tag = new ViewTag();
            _tag.name = (TextView) _view.findViewById(R.id.tv_name);
            _tag.icon = (ImageView) _view.findViewById(R.id.image_icon);
            _view.setTag(_tag);
        }else{
            _tag = (ViewTag) _view.getTag();
        }
        _tag.name.setText(_menu.getName());
        if(_menu.getIcon()>0) {
            _tag.icon.setImageResource(_menu.getIcon());
        }
        return _view;
    }
    private static class ViewTag{
        public TextView name;
        public ImageView icon;
    }
}
