package com.darryring.fast.adapter;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.darryring.fast.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hljdrl on 16/3/18.
 */
public class ImagePagerAdapter extends PagerAdapter {
    private List<View> list;

    private List<String>  imageUris = new ArrayList<String>();
    ImageLoader imageLoader;
    DisplayImageOptions options;
    public ImagePagerAdapter(List<View> list,List<String> _imageUris) {
        this.list = list;
        imageUris.addAll(_imageUris);
        imageLoader = ImageLoader.getInstance();
        options = new DisplayImageOptions.Builder().cacheOnDisk(true).build();
    }

    @Override
    public int getCount() {

        if (list != null && list.size() > 0) {
            return list.size();
        } else {
            return 0;
        }
    }

    @Override
    public boolean isViewFromObject(View arg0, Object arg1) {
        return arg0 == arg1;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View _view = list.get(position);
        final ImageView _imageView = (ImageView) _view.findViewById(R.id.item_imageview);
        container.addView(list.get(position));
        if(position<imageUris.size()) {
            final String _uri = imageUris.get(position);
            imageLoader.displayImage(_uri, _imageView,options);
        }
        return list.get(position);
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

}
