package com.darryring.fast.activitys;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.darryring.fast.R;
import com.darryring.fast.adapter.BrowserAdapter;
import com.darryring.fast.base.BaseAppCompatActivity;
import com.darryring.fast.theme.FastTheme;
import com.darryring.libcore.Fast;
import com.darryring.libmodel.entity.theme.ThemeEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * 浏览器页面
 */
public class BrowserActivity extends BaseAppCompatActivity implements AdapterView.OnItemClickListener {


    /**
     * @param _aty
     * @param _url
     * @return
     */
    public static Intent buildIntent(Activity _aty, String _url) {
        Intent _mIm = new Intent(_aty, BrowserActivity.class);
        _mIm.putExtra(EXTRA_URL, _url);
        return _mIm;
    }

    public static final String EXTRA_URL = "extra_url";

    private String TAG = "BrowserActivity";
    WebView mWebView;
    ProgressBar mProgressBar;
    DrawerLayout drawer;
    ListView mListView;
    BrowserAdapter mAdapter;
    List<String> mUris = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FastTheme.fastTheme.setupTheme(this, ThemeEntity.TYPE_BROWSER_NOACTION_BAR);
        super.onCreate(savedInstanceState);
        Fast.logger.i(TAG, "onCreate....");
        setContentView(R.layout.activity_browser);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        //
        mWebView = (WebView) findViewById(R.id.webview_browser);
        mProgressBar = (ProgressBar) findViewById(R.id.webview_progressbar);
        mListView = (ListView) findViewById(R.id.nav_listview);
        Intent mIm = getIntent();
        String _url = mIm.getStringExtra(EXTRA_URL);

        WebSettings _settings = mWebView.getSettings();
        _settings.setJavaScriptEnabled(true);
        _settings.setJavaScriptCanOpenWindowsAutomatically(true);
        _settings.setGeolocationEnabled(true);
        _settings.setAppCacheEnabled(true);
        _settings.setLoadsImagesAutomatically(true);
        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                if (mUris.indexOf(url) == -1) {
                    mUris.add(url);
                    if (mAdapter != null) {
                        mAdapter.notifyDataSetChanged();
                    }
                }
                return true;
            }
        });
        mWebView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                if (newProgress == 100) {
                    mProgressBar.setVisibility(View.GONE);
                } else {
                    if (mProgressBar.getVisibility() == View.GONE)
                        mProgressBar.setVisibility(View.VISIBLE);
                    mProgressBar.setProgress(newProgress);
                }
                super.onProgressChanged(view, newProgress);
            }
        });
        mWebView.loadUrl(_url);
        mUris.add(_url);
        mAdapter = new BrowserAdapter(this, mUris);
        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(this);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && mWebView.canGoBack()) {
            String _url = mWebView.getUrl();
            mWebView.goBack(); // goBack()表示返回WebView的上一页面
            if (mUris.indexOf(_url) != -1) {
                mUris.remove(_url);
                if (mAdapter != null) {
                    mAdapter.notifyDataSetChanged();
                }
            }
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Fast.logger.i(TAG, "onPause....");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Fast.logger.i(TAG, "onDestroy....");
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String mUri = mUris.get(position);
        if (!mUri.equals(mWebView.getUrl())) {
            mWebView.loadUrl(mUri);
        }
        drawer.closeDrawer(GravityCompat.START);
    }
}
