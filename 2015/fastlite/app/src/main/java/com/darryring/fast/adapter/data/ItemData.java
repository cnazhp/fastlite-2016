package com.darryring.fast.adapter.data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hljdrl on 16/3/17.
 */
public class ItemData {
    private String tag;
    private int gridCount;
    private List<String> data = new ArrayList<>();

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public List<String> getData() {
        return data;
    }

    public void setData(List<String> data) {
        this.data = data;
    }

    public int getGridCount() {
        return gridCount;
    }

    public void setGridCount(int gridCount) {
        this.gridCount = gridCount;
    }
}
