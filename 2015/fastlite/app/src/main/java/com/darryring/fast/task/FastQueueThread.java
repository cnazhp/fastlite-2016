package com.darryring.fast.task;

import android.util.Log;

import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by hljdrl on 16/7/9.
 */
public class FastQueueThread extends Thread{

    private boolean running = false;

    LinkedBlockingQueue<Runnable> runnables = new LinkedBlockingQueue<Runnable>();

    public void postRunnable(Runnable runnable){
        runnables.add(runnable);
    }
    public void quit(){
        running = false;
        postRunnable(mQUIT);
    }
    private final Runnable mQUIT =  new Runnable() {
        @Override
        public void run() {
        }
    };
    @Override
    public void run() {
        running = true;
        while (running) {
            Log.i(getName(),"run...");
            try {
                Runnable _run = runnables.take();
                long _time = System.currentTimeMillis();
                _run.run();
                long _endtime = System.currentTimeMillis();
                Log.i(getName(),"run...time-->"+(_endtime-_time));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
