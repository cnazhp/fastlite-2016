package com.darryring.libmodel.entity;

/**
 * Created by hljdrl on 16/4/12.
 */
public class FMessage {

    public static final int TYPE_TEXT = 1;
    public static final int TYPE_IMAGE = 2;
    public static final int TYPE_AUTDIO = 3;
    public static final int TYPE_VIDEO = 4;
    public static final int TYPE_READ_PACKET = 5;
    public static final int TYPE_LOCATION = 6;
    public static final int TYPE_FILE = 7;
    public static final int TYPE_SYSTEM_MESSAGE = 0;

    /**
     * 发送者id
     */
    private String fromId;
    /**
     * 当前用户id
     */
    private String selfId;
    /**
     * 发送时间，时间戳
     */
    private Long date = System.currentTimeMillis();

    private int type;

    private String message;

    private String msgId;

    public String getFromId() {
        return fromId;
    }

    public void setFromId(String fromId) {
        this.fromId = fromId;
    }

    public String getSelfId() {
        return selfId;
    }

    public void setSelfId(String selfId) {
        this.selfId = selfId;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMsgId() {
        return msgId;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }
}
